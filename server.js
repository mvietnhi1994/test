var express = require("express");
var app     = express();
var path    = require("path");
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var privateKey = '37LvDSm4XvjYOh9Y';
var bodyParser = require('body-parser');
var session = require('express-session');
var cookieParser = require('cookie-parser');

///////////////////////////////////////////////////////////
var index = require('./routes/index');
var users = require('./routes/users');


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/login',function(req,res){
  res.sendFile(path.join(__dirname+'/login.html'));
  //__dirname : It will resolve to your project folder.
});

app.post('/login', function(req, res) {
	//console.log(req.body.username);
	if (req.body.username === "admin" && req.body.password === '123')
	{
		//res.send('You sent the name "' + req.body.name + '".');
		var data = {
			username: "admin",
			password: "123",
			role: "admin"
		};
		var token = jwt.sign(data,privateKey);
		dataLogin = {
			success: true, 
			token: token
		}

		//req.session.tokens = token;
		//console.log(req.session.tokens);
		//res.redirect('/admin');
	} else {
		dataLogin= {
			success: false,
			message: "Invalid username or password"
		}
		//res.render(path.join(__dirname+'/login.html'), {message: "Invalid credentials!"});
		// res.render('login', {message: "Please enter both id and password"});
	}
		res.send(dataLogin);
});

app.get('/admin',function(req,res){
  //res.sendFile(path.join(__dirname+'/admin.html'));
  //__dirname : It will resolve to your project folder.
  //res.render('admin', {message: '"Username: "+"Role:"'});
});

app.post('/admin',function(req,res){
  //res.sendFile(path.join(__dirname+'/admin.html'));
  //__dirname : It will resolve to your project folder.
  //res.render('admin', {message: '"Username: "+"Role:"'});
  var token = req.body.token;
  // decode token
  if (token) {
    // verifies secret and checks exp
    jwt.verify(token, privateKey, function(err, decoded) { 
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' });    
      } else {
        // if everything is good, save to request for use in other routes
		var dataAdmin= {
			success: true,
			data:{
			username: decoded.username,
			role: decoded.role	
			}
		}
        res.send(dataAdmin); 
      }
    });

  } else {
    // if there is no token
    // return an error
    return res.status(403).send({ 
        success: false, 
        message: 'No token provided.' 
    });
  }
});

app.listen(3000);

console.log("Running at Port 3000");